/*
package com.jottery.jottery.view;

import com.jottery.jottery.model.Award;
import com.jottery.jottery.model.LevelItem;
import com.jottery.jottery.model.LevelItemDto;
import com.jottery.jottery.repository.AwardRepository;
import com.jottery.jottery.repository.LevelItemRepository;
import com.jottery.jottery.repository.PersonRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@Route("")
public class MainView extends VerticalLayout {
    private PersonRepository personRepository;
    private LevelItemRepository levelItemRepository;
    private AwardRepository awardRepository;
    private Collection<LevelItem> levelItemList;
    private Collection<Award> awardList;

    @Autowired
    public MainView(PersonRepository personRepository, LevelItemRepository levelItemRepository,AwardRepository awardRepository) {
        this.personRepository = personRepository;
        this.levelItemRepository = levelItemRepository;
        this.awardRepository = awardRepository;
        levelItemList = (Collection<LevelItem>) levelItemRepository.findAll();
        awardList = (Collection<Award>) awardRepository.findAll();

        Label number = new Label("Number of customers: "+personRepository.count());
        TextField nameTextField = new TextField();
        nameTextField.setLabel("Name");
        NumberField minTextField = new NumberField();
        minTextField.setLabel("min");
        NumberField maxTextField = new NumberField();
        maxTextField.setLabel("max");
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        HorizontalLayout nameContainer = new HorizontalLayout();
        nameContainer.add(nameTextField);
        nameContainer.setWidth("100px");
        HorizontalLayout minContainer = new HorizontalLayout();
        minContainer.add(minTextField);
        minContainer.setWidth("100px");
        HorizontalLayout maxContainer = new HorizontalLayout();
        maxContainer.add(maxTextField);
        maxContainer.setWidth("100px");
        Grid<LevelItem> levelItemGrid = new Grid<>();
        levelItemGrid.setHeightByRows(true);
        levelItemGrid.addColumn(LevelItem::getName).setHeader("Label").setSortable(true);
        levelItemGrid.addColumn(LevelItem::getMin).setHeader("Min").setSortable(true);
        levelItemGrid.addColumn(LevelItem::getMax).setHeader("Max").setSortable(true);

        Div addAwardsDiv = new Div();
        addAwardsDiv.setSizeFull();
        Grid<Award> awardGrid = new Grid<>();
        awardGrid.setSizeFull();
        awardGrid.setHeightByRows(true);
        awardGrid.addColumn(Award::getName).setHeader("Award");
        awardGrid.addColumn(Award::getLevelItem).setHeader("Level");
        H3 h3 = new H3("Add Awards");
        TextField awardName = new TextField();
        awardName.setLabel("Award");
        Select<LevelItem> select = new Select<>();
        select.setLabel("Level");

        HorizontalLayout awardsHorizontalLayout = new HorizontalLayout();
        awardsHorizontalLayout.setSizeFull();
        Button button = new Button("Add",event->submitAward(
                awardName.getValue(),
                select.getValue(),
                awardGrid));
        awardsHorizontalLayout.add(awardName);
        awardsHorizontalLayout.add(select);
        awardsHorizontalLayout.add(button);
        addAwardsDiv.add(h3,awardGrid,awardsHorizontalLayout);
        showAwardGrid(awardGrid);

        Button submit = new Button("Submit",event->submitData(
                nameTextField.getValue(),
                minTextField.getValue(),
                maxTextField.getValue(),
                levelItemGrid,
                select
                ));
        HorizontalLayout btnContainer = new HorizontalLayout();
        btnContainer.add(submit);
        horizontalLayout.addAndExpand(nameContainer,minContainer,maxContainer,btnContainer);
        horizontalLayout.setSizeFull();

        Grid<LevelItemDto> processGrid = new Grid<>();
        processGrid.setHeightByRows(true);
        processGrid.addColumn(LevelItemDto::getLevelItemName).setHeader("Label").setSortable(true);
        processGrid.addColumn(LevelItemDto::getNumber).setHeader("Number").setSortable(true);
        Button process = new Button("Process",event->process(processGrid));
        HorizontalLayout processContainer = new HorizontalLayout();
        processContainer.add(process);
        processContainer.setMargin(true);
        processContainer.setPadding(true);
        processContainer.setSizeFull();
        add(new H3("Customers info"),
                number,
                horizontalLayout,
                levelItemGrid,
                processContainer,
                processGrid,
                addAwardsDiv);
        showGrid(levelItemGrid);
        updateSelect(select);
    }

    private void submitAward(String awardName,LevelItem levelItem,Grid<Award> awardGrid) {
        Award award = new Award(awardName,levelItem);
        awardRepository.save(award);
        awardList.add(award);
        showAwardGrid(awardGrid);
    }

    private void updateSelect(Select<LevelItem> select) {
        select.setItemLabelGenerator(LevelItem::getName);
        select.setItems(levelItemList);
    }

    private void showAwardGrid(Grid<Award> grid){
        grid.setDataProvider(DataProvider.ofCollection(awardList));
    }

    private void process(Grid<LevelItemDto> processGrid) {
        for (LevelItem levelItem:levelItemList){
            personRepository.updateLevel(levelItem.getId(),(int) Math.round(levelItem.getMin()),(int) Math.round(levelItem.getMax()));
        }
        List<LevelItemDto> levelItemDtoList = personRepository.countByLevelItem();
        processGrid.setDataProvider(DataProvider.ofCollection(levelItemDtoList));
    }

    private void submitData(String name, Double min, Double max, Grid<LevelItem> grid, Select<LevelItem> select) {
        LevelItem levelItem = new LevelItem();
        levelItem.setName(name);
        levelItem.setMin(min);
        levelItem.setMax(max);
        levelItemRepository.save(levelItem);
        levelItemList.add(levelItem);
        updateSelect(select);
        showGrid(grid);
    }

    private void showGrid(Grid<LevelItem> grid){
        grid.setDataProvider(DataProvider.ofCollection(levelItemList));
    }
}
*/
