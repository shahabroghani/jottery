package com.jottery.jottery.repository;

import com.jottery.jottery.model.LevelItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LevelItemRepository extends CrudRepository<LevelItem,Long> {
}
