package com.jottery.jottery.repository;

import com.jottery.jottery.model.LevelItemDto;
import com.jottery.jottery.model.Person;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends CrudRepository<Person,Long> {
    @Transactional
    @Modifying
    @Query("UPDATE Person p SET p.levelItem.id= :id where p.points BETWEEN :min AND :max")
    void updateLevel(@Param("id") Long id,@Param("min") Integer min, @Param("max") Integer max);

    @Query(value = "SELECT new com.jottery.jottery.model.LevelItemDto(COUNT(p),p.levelItem.name) FROM Person p GROUP BY p.levelItem.id")
    List<LevelItemDto> countByLevelItem();

    Person findPersonByCustomerCode(String code);

}
