package com.jottery.jottery.repository;

import com.jottery.jottery.model.Award;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AwardRepository extends CrudRepository<Award,Long> {
}
