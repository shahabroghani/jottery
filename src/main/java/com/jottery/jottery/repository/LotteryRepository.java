package com.jottery.jottery.repository;

import com.jottery.jottery.model.Lottery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LotteryRepository extends CrudRepository<Lottery,Long> {
}
