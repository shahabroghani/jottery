package com.jottery.jottery.controller;

import com.jottery.jottery.model.LevelItem;
import com.jottery.jottery.repository.LevelItemRepository;
import com.jottery.jottery.repository.PersonRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {
    private PersonRepository personRepository;
    private LevelItemRepository levelItemRepository;

    public IndexController(PersonRepository personRepository, LevelItemRepository levelItemRepository) {
        this.personRepository = personRepository;
        this.levelItemRepository = levelItemRepository;
    }

    @GetMapping({"","/","/index","/home"})
    public String getMapping(Model model){
        model.addAttribute(new LevelItem());
        model.addAttribute("levels",levelItemRepository.findAll());
        model.addAttribute("levelDTO",personRepository.countByLevelItem());
        model.addAttribute("numberOfCustomers",personRepository.count());
        return "index";
    }

    @PostMapping("/levelFormHandle")
    public String postMapping(@ModelAttribute LevelItem levelItem){
        levelItemRepository.save(levelItem);
        return "redirect:";
    }

    @PostMapping("/process")
    public String process(){
        Iterable<LevelItem> levelItems = levelItemRepository.findAll();
        for (LevelItem levelItem:levelItems){
            personRepository.updateLevel(levelItem.getId(),(int) Math.round(levelItem.getMin()),(int) Math.round(levelItem.getMax()));
        }
        return "redirect:";
    }
}
