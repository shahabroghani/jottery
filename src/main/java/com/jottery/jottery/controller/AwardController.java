package com.jottery.jottery.controller;

import com.jottery.jottery.model.Award;
//import com.jottery.jottery.model.LevelItem;
import com.jottery.jottery.repository.AwardRepository;
import com.jottery.jottery.repository.LevelItemRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AwardController {
    private AwardRepository awardRepository;
    private LevelItemRepository levelItemRepository;

    public AwardController(AwardRepository awardRepository,LevelItemRepository levelItemRepository) {
        this.awardRepository = awardRepository;
        this.levelItemRepository = levelItemRepository;
    }

    @GetMapping({"/Award","/award","/awards"})
    public String getMapping(Model model){
        model.addAttribute(new Award());
        model.addAttribute("awards",awardRepository.findAll());
        model.addAttribute("levels",levelItemRepository.findAll());
//        Iterable<LevelItem> levelItems = levelItemRepository.findAll();
        return "awards";
    }

    @PostMapping("/awardFormHandle")
    public String postMapping(@ModelAttribute Award award){
        awardRepository.save(award);
        return "redirect:/award";
    }
}
