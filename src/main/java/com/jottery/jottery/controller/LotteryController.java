package com.jottery.jottery.controller;

import com.jottery.jottery.model.Person;
import com.jottery.jottery.repository.PersonRepository;
import com.jottery.jottery.service.LotteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class LotteryController {
    private LotteryService lotteryService;
    private PersonRepository personRepository;
    private List<String> chance;
    private List<Person> people;

    @Autowired
    public LotteryController(LotteryService lotteryService,PersonRepository personRepository) {
        this.lotteryService = lotteryService;
        this.personRepository = personRepository;
    }

    @GetMapping({"/Lottery","/lottery","Jottery","jottery","/Lot","/lot"})
    public String getLottery(Model model){
        people =(List<Person>) personRepository.findAll();
        chance = lotteryService.getChance(people);
        model.addAttribute("numberOfCustomers",personRepository.count());
        model.addAttribute("chance",chance.size());
        return "lottery";
    }

    @GetMapping("/step")
    public String setStep(@RequestParam(name = "step") Integer step){
        lotteryService.lottery(people,chance,step);
        return "redirect:/Lottery";
    }
}
