package com.jottery.jottery.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Lottery {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String customerCode;
    @ManyToOne
    private Award award;
}
