package com.jottery.jottery.model;

public class LevelItemDto  {
    private Long number;
    private String levelItemName;

    public LevelItemDto(Long number,String levelItemName) {
        this.number = number;
        this.levelItemName = levelItemName;
    }

    public Long getNumber() {
        return number;
    }

    public String getLevelItemName() {
        return levelItemName;
    }
}
