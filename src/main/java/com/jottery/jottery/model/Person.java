package com.jottery.jottery.model;

import lombok.*;

import javax.persistence.*;

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private String customerCode;

    private String idCode;

    private String firstName;

    private String lastName;

    private String birthDate;

    private Integer points;

    private String gender;

    private String phoneNumber;
    private Boolean win;
    @ManyToOne
    private LevelItem levelItem;
}
