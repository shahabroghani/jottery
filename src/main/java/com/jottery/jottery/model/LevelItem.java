package com.jottery.jottery.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class LevelItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;
    @NotNull
    private Double min;
    @NotNull
    private Double max;
    @Override
    public String toString() {
        return name;
    }
}
