package com.jottery.jottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class JotteryApplication {

    public static void main(String[] args) {
        SpringApplication.run(JotteryApplication.class, args);
    }

}
