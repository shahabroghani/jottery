package com.jottery.jottery.service;

import com.jottery.jottery.model.Award;
import com.jottery.jottery.model.Lottery;
import com.jottery.jottery.model.Person;
import com.jottery.jottery.repository.AwardRepository;
import com.jottery.jottery.repository.LevelItemRepository;
import com.jottery.jottery.repository.LotteryRepository;
import com.jottery.jottery.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LotteryService {
    private AwardRepository awardRepository;
    private LevelItemRepository levelItemRepository;
    private PersonRepository personRepository;
    private LotteryRepository lotteryRepository;

    @Autowired
    public LotteryService(AwardRepository awardRepository, LevelItemRepository levelItemRepository, PersonRepository personRepository, LotteryRepository lotteryRepository) {
        this.awardRepository = awardRepository;
        this.levelItemRepository = levelItemRepository;
        this.personRepository = personRepository;
        this.lotteryRepository = lotteryRepository;
    }

    public void lottery(List<Person> people, List<String> chance, Integer step) {
        Collections.shuffle(people);
        Integer chanceSize = chance.size();
        int randNumber = firstRandomNumber(chanceSize);
        List<Award> awards = (List<Award>) awardRepository.findAll();
        awards.sort(new Comparator<Award>() {
            @Override
            public int compare(Award o1, Award o2) {
                return o2.getLevelItem().getMin().compareTo(o1.getLevelItem().getMin());
            }
        });
        for (Award award : awards) {
            while (award.getNumber() > 0){
                    randNumber+=step;
                    if (randNumber>=chanceSize){
                        randNumber %=chanceSize;
                    }
                    String tmpCustomerCode = chance.get(randNumber);
                    if (checkLot(tmpCustomerCode, award)){
                        award.setNumber(award.getNumber()-1);
                    }
            }
        }
    }

    private Boolean checkLot(String customerCode, Award award) {
        Person person = personRepository.findPersonByCustomerCode(customerCode);
        if (person.getPoints() >= award.getLevelItem().getMin() && person.getWin()==null) {
            person.setWin(true);
            System.out.println(person);
            System.out.println(award);
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
            personRepository.save(person);
            Lottery lottery = new Lottery();
            lottery.setAward(award);
            lottery.setCustomerCode(customerCode);
            lotteryRepository.save(lottery);
            return true;
        }
        return false;
    }

    private Integer firstRandomNumber(Integer range) {
        Integer leftLimit = 0;
        Integer rightLimit = range - 1;
        return (int) (leftLimit + (Math.random() * (rightLimit - leftLimit)));
    }

    public List<String> getChance(List<Person> people) {
        List<String> chance = new ArrayList<>();
        for (Person person : people) {
            for (int i = 0; i < person.getPoints(); i++) {
                chance.add(person.getCustomerCode());
            }
        }
        return chance;
    }

}
