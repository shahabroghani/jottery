package com.jottery.jottery.service;

import com.jottery.jottery.model.Award;
import com.jottery.jottery.model.LevelItem;
import com.jottery.jottery.model.Person;
import com.jottery.jottery.repository.AwardRepository;
import com.jottery.jottery.repository.LevelItemRepository;
import com.jottery.jottery.repository.LotteryRepository;
import com.jottery.jottery.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Configuration
@PropertySource(value = "classpath:application.properties")
public class StartUp {
    @Autowired
    private AwardRepository awardRepository;
    @Autowired
    private LevelItemRepository levelItemRepository;
    @Value("${importData}")
    private Boolean importData;
    @Value("${wipeData}")
    private Boolean wipeData;
    @Value("${csvAddress}")
    private String csvAddress;

    private PersonRepository personRepository;
    private Validator validator;
    public StartUp(PersonRepository personRepository,Validator validator) {
        this.personRepository = personRepository;
        this.validator = validator;
    }

    @PostConstruct
    private void init() throws IOException {
        if (wipeData){
            dropDB();
        }
        if (importData){
            readData(csvAddress);
        }
        LevelItem levelItemA = new LevelItem(null,"A",1300D,2000D);
        LevelItem levelItemB = new LevelItem(null,"B",900D,1299D);
        LevelItem levelItemC = new LevelItem(null,"C",300D,899D);
        LevelItem levelItemD = new LevelItem(null,"D",20D,299D);
        levelItemRepository.save(levelItemA);
        levelItemRepository.save(levelItemB);
        levelItemRepository.save(levelItemC);
        levelItemRepository.save(levelItemD);

        Award award1 = new Award(null,"benz",2,levelItemA);
        Award award2 = new Award(null,"pride",5,levelItemB);
//        Award award3 = new Award(null,"BMW",2,levelItemA);
        Award award4 = new Award(null,"coin",10,levelItemC);
        Award award5 = new Award(null,"yaroo",10,levelItemD);
        awardRepository.save(award1);
        awardRepository.save(award2);
//        awardRepository.save(award3);
        awardRepository.save(award4);
//        awardRepository.save(award5);
    }

    private void dropDB() {
        personRepository.deleteAll();
    }

    private void readData(String fileAddress) throws IOException {
        FileReader reader = new FileReader(fileAddress);
        BufferedReader csvReader = new BufferedReader(reader);
        String row ;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            Person person = new Person();
            person.setCustomerCode(data[0]);
            person.setIdCode(data[1]);
            person.setFirstName(data[2]);
            person.setLastName(data[3]);
            person.setPoints(Integer.parseInt(data[4]));
            person.setBirthDate(data[5]);
            person.setGender(data[6]);
            person.setPhoneNumber(data[7]);
            if (validator.test(person))
                personRepository.save(person);
        }
        csvReader.close();
        reader.close();
    }
}
